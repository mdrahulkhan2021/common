package pathfilter_test

import (
	"fmt"
	"os"

	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/pathfilter"
)

func Example() {

	app := &cli.App{
		Name: "scan",

		// initialize flags with SCAN_ prefix
		Flags: pathfilter.MakeFlags("SCAN_"),

		// scan function
		Action: func(c *cli.Context) error {

			// initialize path filter
			filter, err := pathfilter.NewFilter(c)
			if err != nil {
				return err
			}

			// show files being scanned
			for _, path := range c.Args().Slice() {
				if !filter.IsExcluded(path) {
					fmt.Println("scanning file:", path)
				}
			}

			return nil
		},
	}

	// configure path filter using environment variables;
	// it can also be configured using CLI flags
	os.Setenv("SCAN_EXCLUDED_PATHS", "spec,test,tmp")

	os.Args = []string{"scan",
		"src/main/java/com/gitlab/app/App.java",
		"src/main/java/com/gitlab/app/Config.java",
		"src/test/java/com/gitlab/app/AppTest.java",
		"src/test/java/com/gitlab/app/ConfigTest.java",
	}

	app.Run(os.Args)
	// Output:
	// scanning file: src/main/java/com/gitlab/app/App.java
	// scanning file: src/main/java/com/gitlab/app/Config.java
}

func ExampleFilter_IsExcluded() {
	filter := pathfilter.Filter{
		ExcludedPaths: []string{"spec", "test", "tmp"},
	}
	fmt.Println(filter.IsExcluded("src/test/java/com/gitlab/tests/AppTest.java"))
	// Output: true
}

func ExampleFilter_String() {
	filter := pathfilter.Filter{
		ExcludedPaths: []string{"spec", "test", "tmp"},
	}
	fmt.Println(filter.String())
	// Output: spec,test,tmp
}

func ExampleMatch_filename() {
	match, _ := pathfilter.Match("AppTest.java", "src/test/java/com/gitlab/tests/AppTest.java")
	fmt.Println(match)
	// Output: true
}

func ExampleMatch_directory() {
	match, _ := pathfilter.Match("tests", "src/test/java/com/gitlab/tests/AppTest.java")
	fmt.Println(match)
	// Output: true
}

func ExampleMatch_ancestor() {
	match, _ := pathfilter.Match("gitlab", "src/test/java/com/gitlab/tests/AppTest.java")
	fmt.Println(match)
	// Output: true
}

func ExampleMatch_absolute() {
	match, _ := pathfilter.Match("/src/test", "src/test/java/com/gitlab/tests/AppTest.java")
	fmt.Println(match)
	// Output: true
}
