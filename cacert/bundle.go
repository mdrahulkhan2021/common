package cacert

import (
	"fmt"
	"os"
	"path/filepath"
)

var gitconfigPath = "/etc/gitconfig"

// bundle combines a CA bundle with the path where it is imported.
type bundle struct {
	content string // content is the CA bundle to import.
	path    string // path is where the CA bundle is imported.
}

// Write writes the CA bundle to its import path.
// This is a no-op if the CA bundle is empty.
func (b bundle) write() error {
	// skip if bundle is empty
	if b.content == "" {
		return nil
	}

	// skip if no import path
	if b.path == "" {
		return nil
	}

	if err := b.writeCACertFile(); err != nil {
		return err
	}

	if err := b.writeCACertPathToGitConfigFile(); err != nil {
		return err
	}

	return nil
}

func (b bundle) writeCACertFile() error {
	// create parent directory for the import path
	if err := os.MkdirAll(filepath.Dir(b.path), 0755); err != nil {
		return err
	}

	// If the file doesn't exist, create it, or append to the file
	f, err := os.OpenFile(b.path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = fmt.Fprintln(f, b.content)
	return err
}

// writeCACertPathToGitConfigFile configures the `sslCAInfo` value in the `/etc/gitconfig` file
// to point to the certificate file provided by the bundle. Git will use the certificate file
// pointed to from the `sslCAInfo` value to verify the peer with when fetching or pushing over HTTPS
// see https://git-scm.com/docs/git-config#Documentation/git-config.txt-httpsslCAInfo for more info
// Note: we could also write this path value to the `GIT_SSL_CAINFO` environment variable, but that
// would require all code that makes use of git to import the environment variables before executing
// the git process.
func (b bundle) writeCACertPathToGitConfigFile() error {
	gitconfigContent := fmt.Sprintf("[http] sslCAInfo = %s", b.path)

	// If the file doesn't exist, create it, or append to the file
	f, err := os.OpenFile(gitconfigPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	// each time the analyer is run, the gitconfigContent string will be appended to the /etc/gitconfig
	// file, meaning that we'll end up with duplicate entries in the file if the analyzer is run twice,
	// for example, during local development in a running Docker container. We need to use Fprintln here
	// so that each new entry appears on a separate line instead of being directly concatenated together.
	// We could change the logic to check for the existence of the gitconfigContent string before appending
	// a new one, but it increases complexity, and since git is able to handle an /etc/gitconfig file with
	// multiple duplicate entries, this shouldn't be a concern.
	_, err = fmt.Fprintln(f, gitconfigContent)
	return err
}
